import { Component, OnInit } from '@angular/core';
import participantes from '../../../participantes.json';

@Component({
  selector: 'app-participantes',
  templateUrl: './participantes.component.html',
  styleUrls: ['./participantes.component.css']
})
export class ParticipantesComponent implements OnInit {
   participou = []
    
  constructor() { }

  ngOnInit() {
    this.participou.push(participantes)    
    
  }

}
